package com.pilipili.common.util;

import com.pilipili.common.Exception.BusinessRuntimeException;
import com.pilipili.common.Exception.CommonErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 处理放射相关的辅助类
 * 
 * @author deng.huaiyu
 *
 */
public class ReflectUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ReflectUtils.class);
	
	/**
	 * 创建对象
	 * 
	 * @param clazz
	 * @return
	 */
	public static  <T> T newInstance(Class<T> clazz){
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			logger.error(" newInstance error ", e);
			throw new BusinessRuntimeException(CommonErrorCode.UNKOWN);
		}
	}
}
