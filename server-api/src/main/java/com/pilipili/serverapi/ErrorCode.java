package com.pilipili.serverapi;


import com.pilipili.common.Exception.CommonErrorCode;
import net.sf.saxon.trans.Err;

/**
 * 后台错误码
 *
 * @author liang.hongji
 */
public class ErrorCode extends CommonErrorCode {

    public static final ErrorCode USER_NOT_FOUND = new ErrorCode(1000, "用户不存在或密码错误");
    public static final ErrorCode PASSWORD_INCONSISTENT = new ErrorCode(1001, "两次输入密码不一致");
    public static final ErrorCode INVAILD_TYPE = new ErrorCode(1002, "注册类型非法");
    public static final ErrorCode EXSIST_USER = new ErrorCode(1003, "用户已存在");
    public static final ErrorCode USER_NOT_LOGNIN = new ErrorCode(1004, "用户未登录或登录已过期");
    public static final ErrorCode ROLE_NOT_FOUND = new ErrorCode(1005, "角色不存在");
    public static final ErrorCode ROLE_EXITS = new ErrorCode(1006, "角色已存在");
    public static final ErrorCode UNAUTH = new ErrorCode(1007, "没有权限访问");
    public static final ErrorCode TOKEN_INVAILD = new ErrorCode(1008, "登录状态已失效");
    public static final ErrorCode SHIRO_ERROR = new ErrorCode(1009, "shiro发生错误");
    public static final ErrorCode USER_NAME_EXIST = new ErrorCode(1010, "用户名已存在");
    public static final ErrorCode PERMISSION_EXIST = new ErrorCode(1011, "权限已存在");
    public static final ErrorCode PERMISSION_NOT_FOUND = new ErrorCode(1012, "权限不存在");
    public static final ErrorCode VIDOEO_TITLE_EXIST = new ErrorCode(1013, "视频标题已存在");
    public static final ErrorCode VIDEO_ORI_EXIST = new ErrorCode(1014, "请不要重复上传文件名相同的文件");
    public static final ErrorCode VIDEO_DELETE_REJECT = new ErrorCode(1015, "不能删除正在转码中的视频");
    public static final ErrorCode VIDEO_AUDIT_ERROR = new ErrorCode(1016, "不能审核未转码完成的视频");
    public static final ErrorCode AUDIT_STATE_ERROR = new ErrorCode(1017, "只有1（通过审核）和0（未审核）和-1（不通过审核）三种状态");
    public static final ErrorCode COLLECTION_EXIST = new ErrorCode(1018, "选中的收藏夹已收藏过该视频");
    public static final ErrorCode FAVORITES_NOT_FOUND = new ErrorCode(1019, "收藏夹不存在");
    public ErrorCode(int code, String message) {
        super(code, message);
    }
}
