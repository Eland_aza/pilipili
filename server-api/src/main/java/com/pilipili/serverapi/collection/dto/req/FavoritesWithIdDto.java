package com.pilipili.serverapi.collection.dto.req;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 收藏夹带id信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "FavoritesWithIdDto", description = "收藏夹带id信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoritesWithIdDto extends FavoritesDto {

    @NotNull(message = "收藏夹id")
    private Long id;

    public FavoritesWithIdDto(Long id, Long userId, String favoritesName, String introduction) {
        super(userId, favoritesName, introduction);
        this.id = id;
    }
}
