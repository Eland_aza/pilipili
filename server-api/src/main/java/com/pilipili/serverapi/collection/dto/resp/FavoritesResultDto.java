package com.pilipili.serverapi.collection.dto.resp;

import com.pilipili.serverapi.video.dto.resp.VideoSearchResultDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 收藏夹结果信息
 *
 * @author chen.guosheng
 *
 */
@ApiModel(value = "FavoritesResultDto", description = "收藏夹结果信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoritesResultDto {

    @ApiModelProperty(value = "收藏夹id")
    private Long id;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "收藏夹名称")
    private String favoritesName;

    @ApiModelProperty(value = "收藏夹简介")
    private String introduction;

    @ApiModelProperty(value = "创建时间")
    private String createTime;
}
