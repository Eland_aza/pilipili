package com.pilipili.serverapi.collection.service;

import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.service.AbstractService;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.collection.dto.resp.CollectionResultDto;
import com.pilipili.serverapi.collection.entity.Collection;
import com.pilipili.serverapi.collection.repository.CollectionRepository;
import com.pilipili.serverapi.video.dto.req.VideoSearchDto;
import com.pilipili.serverapi.video.dto.resp.VideoSearchResultDto;
import com.pilipili.serverapi.video.entity.Video;
import com.pilipili.serverapi.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 收藏条目管理Service
 *
 * @author chen.guosheng
 */
@Service
public class CollectionService extends AbstractService<CollectionRepository, Collection, Long> {

    @Autowired
    private VideoRepository videoRepository;

    @Transactional
    public boolean existCollection(Long userId, Long videoId, Long favoritesId){
        Collection collection = this.repository.findByUserIdAndVideoIdAndFavoritesId(userId, videoId, favoritesId);
        if(collection != null) return true;
        return false;
    }

    @Transactional
    public PageCommResult<CollectionResultDto> search(Long favoritesId, int currentPage, int pageCount){
        Pageable page = new PageRequest(currentPage, pageCount, Sort.Direction.DESC, "createTime");
        List<Collection> collections = this.repository.findByFavoritesId(favoritesId, page);
        List<CollectionResultDto> collectionResultDtos = new ArrayList<CollectionResultDto>();
        for(Collection collection : collections){
            CollectionResultDto collectionResultDto = new CollectionResultDto();
            BeanUtils.copyProperties(collection, collectionResultDto);
            VideoSearchDto videoSearchDto = new VideoSearchDto();
            videoSearchDto.setId(collection.getVideoId());
            List<Video> videos = videoRepository.search(videoSearchDto);
            VideoSearchResultDto videoSearchResultDto = new VideoSearchResultDto();
            if(!videos.isEmpty())BeanUtils.copyProperties(videos.get(0), videoSearchResultDto);
            collectionResultDto.setVideoSearchResultDto(videoSearchResultDto);
            collectionResultDtos.add(collectionResultDto);
        }
        Long total = this.repository.countByUserId(favoritesId);
        return PageCommResult.successPageResult(collectionResultDtos, currentPage, pageCount, total);
    }

    /**
     * 批量删除收藏条目
     *
     * @param collectionIds
     */
    @Transactional
    public void batchDelete(List<Long> collectionIds) {
        // 由于设定了多对多关系，因jpa会自动删除角色与权限的联系记录
        this.repository.deleteByIdIn(collectionIds);
    }

    /**
     * 根据视频id批量删除收藏条目
     *
     * @param videoIds
     */
    @Transactional
    public void batchDeleteByVideoIds(List<Long> videoIds) {
        // 由于设定了多对多关系，因jpa会自动删除角色与权限的联系记录
        this.repository.deleteByVideoIdIn(videoIds);
    }

}
