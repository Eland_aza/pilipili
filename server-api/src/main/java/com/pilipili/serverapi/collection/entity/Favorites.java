package com.pilipili.serverapi.collection.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 收藏夹实体类
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Favorites implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private String favoritesName;
    private String introduction;
    private Date createTime;
}
