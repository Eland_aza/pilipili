package com.pilipili.serverapi.collection.repository;


import com.pilipili.serverapi.collection.entity.Collection;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 收藏管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface CollectionRepository extends JpaRepository<Collection, Long> {


    List<Collection> findByFavoritesId(Long favoritesId, Pageable pageable);

    Collection findByUserIdAndVideoIdAndFavoritesId(Long userId, Long videoId, Long favoritesId);

    Long countByUserId(Long userId);

    /**
     * 批量删除收藏条目
     *
     * @param ids
     */
    void deleteByIdIn(java.util.Collection<Long> ids);

    /**
     * 根据视频id批量删除收藏条目
     *
     * @param ids
     */
    void deleteByVideoIdIn(java.util.Collection<Long> ids);

}
