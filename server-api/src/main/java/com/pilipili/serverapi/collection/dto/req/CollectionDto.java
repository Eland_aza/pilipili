package com.pilipili.serverapi.collection.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 收藏条目信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "CollectionDto", description = "收藏条目信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionDto {


    @ApiModelProperty(value = "用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @ApiModelProperty(value = "视频id")
    @NotNull(message = "视频id不能为空")
    private Long videoId;

    @ApiModelProperty(value = "收藏夹ids")
    @NotNull(message = "收藏夹id不能为空")
    private List<Long> favoritesIds;
}
