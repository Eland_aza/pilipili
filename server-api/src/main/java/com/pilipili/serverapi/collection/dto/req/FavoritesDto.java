package com.pilipili.serverapi.collection.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 收藏夹信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "FavoritesDto", description = "收藏夹信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoritesDto {

    @ApiModelProperty(value = "用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @ApiModelProperty(value = "收藏夹名称")
    @NotBlank
    private String favoritesName;

    @ApiModelProperty(value = "收藏夹简介")
    private String introduction;

}
