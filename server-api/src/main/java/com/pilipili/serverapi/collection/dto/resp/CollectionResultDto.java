package com.pilipili.serverapi.collection.dto.resp;


import com.pilipili.serverapi.video.dto.resp.VideoSearchResultDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 收藏条目结果信息
 *
 * @author chen.guosheng
 *
 */
@ApiModel(value = "CollectionResultDto", description = "收藏条目结果信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionResultDto {

    @ApiModelProperty(value = "收藏条目id")
    private Long id;

    @ApiModelProperty(value = "视频dto")
    private VideoSearchResultDto videoSearchResultDto;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

}
