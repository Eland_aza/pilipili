package com.pilipili.serverapi.collection.repository;

import com.pilipili.serverapi.collection.entity.Favorites;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 收藏夹管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface FavoritesRepository extends JpaRepository<Favorites, Long> {

    List<Favorites> findByUserId(Long userId, Pageable pageable);

    Long countByUserId(Long userId);

    void deleteByIdIn(Collection<Long> ids);

}
