package com.pilipili.serverapi.comment.service;

import com.pilipili.common.service.AbstractService;
import com.pilipili.serverapi.comment.entity.Comment;
import com.pilipili.serverapi.comment.repository.CommentRepository;
import org.springframework.stereotype.Service;

/**
 * 评论管理Service
 *
 * @author chen.guosheng
 */
@Service
public class CommentService extends AbstractService<CommentRepository, Comment, Long> {

}
