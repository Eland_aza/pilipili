package com.pilipili.serverapi.comment.repository;

import com.pilipili.serverapi.comment.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 评论管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

}
