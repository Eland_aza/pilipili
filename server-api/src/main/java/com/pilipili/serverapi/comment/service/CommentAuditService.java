package com.pilipili.serverapi.comment.service;

import com.pilipili.common.service.AbstractService;
import com.pilipili.serverapi.comment.entity.CommentAudit;
import com.pilipili.serverapi.comment.repository.CommentAuditRepository;
import org.springframework.stereotype.Service;

/**
 * 评论审核管理Service
 *
 * @author chen.guosheng
 */
@Service
public class CommentAuditService extends AbstractService<CommentAuditRepository, CommentAudit, Long> {

}
