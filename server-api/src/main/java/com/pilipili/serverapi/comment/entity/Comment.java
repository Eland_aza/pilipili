package com.pilipili.serverapi.comment.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 评论实体类
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long publisherId;
    private String publisherName;
    private Long replyUserId;
    private String replyUserName;
    private Long replyCommentId;
    private String content;
    private Long videoId;
    private Date createTime;

}
