package com.pilipili.serverapi.comment.repository;

import com.pilipili.serverapi.comment.entity.CommentAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 评论审核管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface CommentAuditRepository extends JpaRepository<CommentAudit, Long> {

}
