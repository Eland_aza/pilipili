package com.pilipili.serverapi.manageUser.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
/**
 * 管理用户注册Dto
 *
 * @author chen.guosheng
 */
@Data
@ApiModel(value = "ManageUserSignUpDto", description = "用户注册dto")
public class ManageUserSighUpDto {

    @ApiModelProperty(value = "用户名", required = true)
    @NotNull(message = "必须输入用户名")
    private String userName;

    @ApiModelProperty(value = "手机号", required = true)
    @NotNull(message = "必须输入手机号")
    private String mobile;

    @ApiModelProperty(value = "邮箱", required = true)
    @NotNull(message = "必须输入邮箱")
    private String email;

    @ApiModelProperty(value = "密码", required = true)
    @NotNull(message = "必须输入密码")
    private String password;

    @ApiModelProperty(value = "确认密码", required = true)
    @NotNull(message = "必须输入确认密码")
    private String verifyPassword;

}
