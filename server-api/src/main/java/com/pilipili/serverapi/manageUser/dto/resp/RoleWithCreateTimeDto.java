package com.pilipili.serverapi.manageUser.dto.resp;

import com.pilipili.serverapi.manageUser.dto.req.RoleWithIdDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 角色搜索结果
 *
 * @author chen.guosheng
 *
 */
@ApiModel(value = "RoleWithCreateTimeDto", description = "角色搜索结果")
@Data
@AllArgsConstructor
public class RoleWithCreateTimeDto extends RoleWithIdDto {


    @ApiModelProperty(value = "创建时间")
    private String createTime;

    public RoleWithCreateTimeDto(String name, String description, Long id, String createTime) {
        super(name, description, id);
        this.createTime = createTime;
    }
}
