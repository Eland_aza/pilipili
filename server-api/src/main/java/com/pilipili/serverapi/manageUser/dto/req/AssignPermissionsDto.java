package com.pilipili.serverapi.manageUser.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 分配权限
 *
 * @author chen.guosheng
 */
@ApiModel(value = "AssignPermissionsDto", description = "分配权限")
@Data
@AllArgsConstructor
public class AssignPermissionsDto {

    @ApiModelProperty(value = "角色id，系统唯一", required = true)
    @NotNull(message = "roleId")
    private Long roleId;

    @ApiModelProperty(value = "权限id，系统唯一", required = true)
    @NotNull(message = "permissionIds")
    private List<Long> permissionIds;

}
