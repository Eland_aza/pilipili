package com.pilipili.serverapi.manageUser.dto.req;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 权限信息包括id
 *
 * @author chen.guosheng
 */
@ApiModel(value = "PermissionWithIdDto", description = "权限信息包括id")
@Data
@NoArgsConstructor
public class PermissionWithIdDto extends PermissionDto{
    @NotNull(message = "权限id")
    private Long id;

    public PermissionWithIdDto(String permissionName, String description, String permissionUrl, Long id) {
        super(permissionName, description, permissionUrl);
        this.id = id;
    }
}
