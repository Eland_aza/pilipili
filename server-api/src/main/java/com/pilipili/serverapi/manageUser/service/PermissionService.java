package com.pilipili.serverapi.manageUser.service;

import com.pilipili.common.Exception.CommonErrorCode;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.service.AbstractService;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.manageUser.dto.resp.PermissionWithCreateTimeDto;
import com.pilipili.serverapi.manageUser.entity.Permission;
import com.pilipili.serverapi.manageUser.entity.Role;
import com.pilipili.serverapi.manageUser.repository.PermissionRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 权限管理Service
 *
 * @author chen.guosheng
 */
@Service
public class PermissionService extends AbstractService<PermissionRepository, Permission, Long> {

    @Transactional(readOnly = true)
    public PageCommResult<PermissionWithCreateTimeDto> search(String permissionName, int currentPage, int pageCount) {
        Pageable page = new PageRequest(currentPage, pageCount, Sort.Direction.DESC, "createTime");
        List<Permission> permissions = this.repository.findByPermissionNameStartingWith(permissionName, page);
        List<PermissionWithCreateTimeDto> permissionWithCreateTimeDtos = new ArrayList<PermissionWithCreateTimeDto>();
        permissions.forEach(permission -> permissionWithCreateTimeDtos.add(new PermissionWithCreateTimeDto(permission.getPermissionName(), permission.getDescription(), permission.getPermissionUrl(), permission.getId(), permission.getCreateTime().toString())));
        Long total = this.repository.countByPermissionNameStartingWith(permissionName);
        return PageCommResult.successPageResult(permissionWithCreateTimeDtos, currentPage, pageCount, total);
    }

    @Transactional
    @Override
    public <PermissionWithIdDto> Permission editAfterFind(Long id, PermissionWithIdDto dto, CommonErrorCode errorCode){
        Permission permission = this.find(id, errorCode);
        BeanUtils.copyProperties(dto, permission);
        permission.setCreateTime(new Date());
        return repository.save(permission);
    }

    /**
     * 批量删除权限
     *
     * @param permissionIds
     */
    @Transactional
    public void batchDelete(List<Long> permissionIds) {
        // 由于设定了多对多关系，因jpa会自动删除角色与权限的联系记录
        this.repository.deleteByIdIn(permissionIds);
    }

    /**
     * 根据permissionIds查找权限
     *
     * @param permissionIds
     */
    @Transactional
    public List<Permission> findByIdIn(List<Long> permissionIds){
        return this.repository.findByIdIn(permissionIds);
    }

    /**
     * 根据roles查找权限
     *
     * @param roles
     */
    @Transactional
    public PageCommResult<PermissionWithCreateTimeDto> findByRoles(List<Role> roles, int currentPage, int pageCount){
        Pageable page = new PageRequest(currentPage, pageCount, Sort.Direction.DESC, "createTime");
        List<Permission> permissions = this.repository.findByRoles(roles, page);
        List<PermissionWithCreateTimeDto> permissionWithCreateTimeDtos = new ArrayList<PermissionWithCreateTimeDto>();
        permissions.forEach(permission -> permissionWithCreateTimeDtos.add(new PermissionWithCreateTimeDto(permission.getPermissionName(), permission.getDescription(), permission.getPermissionUrl(), permission.getId(), permission.getCreateTime().toString())));
        Long total = this.repository.countByRoles(roles);
        return PageCommResult.successPageResult(permissionWithCreateTimeDtos, currentPage, pageCount, total);
    }
}
