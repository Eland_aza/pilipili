package com.pilipili.serverapi.manageUser.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 管理用户登录成功返回结果
 * 
 * @author chen.guosheng
 *
 */
@ApiModel(value = "ManageLoginResultDto", description = "管理用户登录成功返回结果")
@Data
@AllArgsConstructor
public class ManageLoginResultDto {

	@ApiModelProperty(value = "用户id")
	private Long userId;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "用户名token")
	private String userAccessToken;

	public ManageLoginResultDto(){

	}
}
