package com.pilipili.serverapi.manageUser.dto.req;

import com.pilipili.serverapi.manageUser.dto.req.RoleDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 角色信息包括id
 *
 * @author chen.guosheng
 */
@ApiModel(value = "RoleWithIdDto", description = "角色信息包括id")
@Data
@NoArgsConstructor
public class RoleWithIdDto extends RoleDto {

    @NotNull(message = "角色名id")
    private Long id;

    public RoleWithIdDto(String name, String description, Long id) {
        super(name, description);
        this.id = id;
    }
}
