package com.pilipili.serverapi.manageUser.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 个人信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "ManageUserInfoResultDto", description = "个人信息")
@Data
@AllArgsConstructor
public class ManageUserInfoDto {

    @ApiModelProperty(value = "用户id，系统唯一", required = true)
    @NotNull(message = "id")
    private Long id;

    @ApiModelProperty(value = "名称，系统唯一", required = true)
    @NotBlank(message = "用户名不为空")
    private String userName;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "电子邮件")
    private String email;

    @ApiModelProperty(value = "密码")
    private String password;

}
