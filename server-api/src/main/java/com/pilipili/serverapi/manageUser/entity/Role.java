package com.pilipili.serverapi.manageUser.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 角色实体类
 * 注意此类不要用@Data注解因为有两个多对多映射，会导致两个集合循环比较爆栈
 * @author chen.guosheng
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String roleName;
    private String description;
    @ManyToMany(mappedBy = "roles",fetch = FetchType.EAGER)
    private Set<ManageUser> manageUsers;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission", joinColumns = {
            @JoinColumn(name = "role_id", referencedColumnName = "ID")}, inverseJoinColumns = {
            @JoinColumn(name = "permission_id", referencedColumnName = "ID")})
    private Set<Permission> permissions;
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ManageUser> getManageUsers() {
        return manageUsers;
    }

    public void setManageUsers(Set<ManageUser> manageUsers) {
        this.manageUsers = manageUsers;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
