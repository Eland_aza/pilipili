package com.pilipili.serverapi.manageUser.dto.resp;

import com.pilipili.serverapi.manageUser.dto.req.PermissionWithIdDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 权限搜索结果
 *
 * @author chen.guosheng
 *
 */
@ApiModel(value = "PermissionWithCreateTimeDto", description = "权限搜索结果")
@Data
@AllArgsConstructor
public class PermissionWithCreateTimeDto extends PermissionWithIdDto {

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    public PermissionWithCreateTimeDto(String name, String description, String permissionUrl, Long id, String createTime) {
        super(name, description, permissionUrl, id);
        this.createTime = createTime;
    }
}
