package com.pilipili.serverapi.manageUser.repository;

import com.pilipili.serverapi.manageUser.entity.Permission;
import com.pilipili.serverapi.manageUser.entity.Role;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


/**
 * 权限管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    /**
     * 根据名模糊查找
     * @param name
     * @param pageable
     * @return
     */
    List<Permission> findByPermissionNameStartingWith(String name, Pageable pageable);

    /**
     * 统计总数
     * @param name
     * @return
     */
    Long countByPermissionNameStartingWith(String name);

    /**
     * 批量删除权限
     *
     * @param ids
     */
    void deleteByIdIn(Collection<Long> ids);

    /**
     * 根据id查找权限
     *
     * @param ids
     */
    List<Permission> findByIdIn(Collection<Long> ids);

    /**
     * 根据role查找权限
     *
     * @param roles
     */
    List<Permission> findByRoles(Collection<Role> roles, Pageable pageable);

    /**
     * 统计总数
     * @param roles
     * @return
     */
    Long countByRoles(Collection<Role> roles);
}
