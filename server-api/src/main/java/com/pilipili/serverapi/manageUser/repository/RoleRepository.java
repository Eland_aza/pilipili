package com.pilipili.serverapi.manageUser.repository;

import com.pilipili.serverapi.manageUser.entity.Role;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 角色管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    /**
     * 根据名模糊查找
     * @param name
     * @param pageable
     * @return
     */
    List<Role> findByRoleNameStartingWith(String name, Pageable pageable);

    /**
     * 统计总数
     * @param name
     * @return
     */
    Long countByRoleNameStartingWith(String name);

    /**
     * 批量删除角色
     *
     * @param ids
     */
    void deleteByIdIn(Collection<Long> ids);
}
