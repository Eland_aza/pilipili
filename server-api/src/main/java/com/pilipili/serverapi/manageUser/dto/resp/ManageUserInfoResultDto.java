package com.pilipili.serverapi.manageUser.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 用户信息
 *
 * @author chen.guosheng
 *
 */
@ApiModel(value = "ManageUserInfoResultDto", description = "用户信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageUserInfoResultDto {

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

}
