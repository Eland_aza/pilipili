package com.pilipili.serverapi.manageUser.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 权限信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "PermissionDto", description = "权限信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionDto {
    @ApiModelProperty(value = "角色名，系统唯一", required = true)
    @NotBlank(message = "权限名不能为空")
    private String permissionName;

    @ApiModelProperty(value = "权限描述")
    private String description;

    @ApiModelProperty(value = "访问url")
    private String permissionUrl;
}
