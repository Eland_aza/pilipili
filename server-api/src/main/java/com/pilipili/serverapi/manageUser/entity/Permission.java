package com.pilipili.serverapi.manageUser.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 系统资源
 *
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Permission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String permissionName;
    private String description;
    private String permissionUrl;
    private Long parentId;
    @ManyToMany(mappedBy = "permissions",fetch = FetchType.EAGER)
    private Set<Role> roles;
    private Date createTime;
}
