package com.pilipili.serverapi.manageUser.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 后台用户实体类
 *
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class ManageUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private String password;
    private String mobile;
    private String email;
    private Date createTime;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = {
            @JoinColumn(name = "user_id", referencedColumnName = "ID")}, inverseJoinColumns = {
            @JoinColumn(name = "role_id", referencedColumnName = "ID")})
    private Set<Role> roles;

    public ManageUser(String userName, String password, String mobile, String email) {
        this.userName = userName;
        this.password = password;
        this.mobile = mobile;
        this.email = email;
    }

    public ManageUser(){

    }
}
