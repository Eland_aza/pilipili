package com.pilipili.serverapi.manageUser.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 角色信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "RoleDto", description = "角色信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto {

    @ApiModelProperty(value = "角色名，系统唯一", required = true)
    @NotBlank(message = "角色名不能为空")
    private String roleName;

    @ApiModelProperty(value = "角色描述")
    private String description;

}
