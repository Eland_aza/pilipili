package com.pilipili.serverapi.manageUser.repository;

import com.pilipili.serverapi.manageUser.entity.ManageUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 用户管理Repository
 *
 * @author chen.guosheng
 */
@Repository
public interface ManageUserRepository extends JpaRepository<ManageUser, Long> {

    ManageUser findByUserName(String userName);
}
