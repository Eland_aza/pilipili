package com.pilipili.serverapi.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * 资源URL匹配路径
 *
 * @author chen.guosheng
 */
//这里请务必继承WebMvcConfigurerAdapter 而不是WebMvcConfigurationSupport！！！！会影响swagger的使用
@Configuration
@PropertySource({"classpath:application.yml"})
public class MyURLPatternConfig extends WebMvcConfigurerAdapter {

    @Value("${savePath.videoFilePath}")
    private String videoFilePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 指定到D盘下的myFile文件夹
        // 注:如果是Linux的话，直接指定文件夹路径即可，不需要指定哪个盘(Linux就一个可用盘)
        registry.addResourceHandler("/videoResource/**").addResourceLocations("file:"+videoFilePath);
        super.addResourceHandlers(registry);
    }
}
