package com.pilipili.serverapi.controller.collection;

import com.pilipili.common.Exception.BusinessRuntimeException;
import com.pilipili.common.controller.AbstractController;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.ErrorCode;
import com.pilipili.serverapi.collection.dto.req.FavoritesDto;
import com.pilipili.serverapi.collection.dto.req.FavoritesWithIdDto;
import com.pilipili.serverapi.collection.dto.resp.FavoritesResultDto;
import com.pilipili.serverapi.collection.entity.Favorites;
import com.pilipili.serverapi.collection.service.FavoritesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 收藏夹管理接口
 *
 * @author chen.guosheng
 */
@Api(description = "收藏夹管理接口")
@RestController
@RequestMapping("/serverapi/favorites")
public class FavoritesController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(FavoritesController.class);

    @Autowired
    private FavoritesService favoritesService;

    @ApiOperation(value = "新增收藏夹")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public PageCommResult<Object> add(@Valid @RequestBody FavoritesDto favoritesDto, BindingResult bindingResult) {
        LOG.info(" access /serverapi/favorites/add favoritesDto {} ", favoritesDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        Favorites favorites = new Favorites();
        BeanUtils.copyProperties(favoritesDto, favorites);
        favoritesService.save(favorites);
        return this.successPageResultWithEmptyResult();
    }

    @ApiOperation(value = "搜索收藏夹")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public PageCommResult<FavoritesResultDto> search(@RequestParam(value = "userId", defaultValue = "") Long userId,
                                                     @RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
                                                     @RequestParam(value = "pageCount", defaultValue = "10") Integer pageCount) {
        LOG.info(" access /serverapi/favorites/search userId {} start {} count {} ", userId, currentPage, pageCount);
        return favoritesService.search(userId, currentPage, pageCount);
    }

    @ApiOperation(value = "编辑收藏夹")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public PageCommResult<Object> edit(@Valid @RequestBody FavoritesWithIdDto favoritesWithIdDto,
                                       BindingResult bindingResult) {
        LOG.info(" access /serverapi/favorites/edit favoritesWithIdDto {} ", favoritesWithIdDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        favoritesService.editAfterFind(favoritesWithIdDto.getId(), favoritesWithIdDto, ErrorCode.FAVORITES_NOT_FOUND);
        return this.successPageResultWithEmptyResult();
    }

    @ApiOperation(value = "批量删除收藏夹")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
    public PageCommResult<Object> batchDelete(@RequestParam("favoritesIds") List<Long> favoritesIds) {
        LOG.info(" access /serverapi/role/batchDelete favoritesIds {} ", favoritesIds);
        favoritesService.batchDelete(favoritesIds);
        return this.successPageResultWithEmptyResult();
    }


}
