package com.pilipili.serverapi.controller.video;


import com.pilipili.common.controller.AbstractController;
import com.pilipili.common.controller.CommonResult;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.serverapi.video.dto.resp.TaskWithVideoTitleDto;
import com.pilipili.serverapi.video.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 视频管理接口
 *
 * @author chen.guosheng
 */
@Api(description = "任务管理接口")
@RestController
@RequestMapping("/serverapi/task")
public class TaskController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(VideoController.class);

    @Autowired
    private TaskService taskService;

    @ApiOperation(value = "搜索任务")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public PageCommResult<TaskWithVideoTitleDto> search(@RequestParam(value = "userId", defaultValue = "") Long userId,
                                                        @RequestParam(value = "state", defaultValue = "-2") Long state,
                                                        @RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
                                                        @RequestParam(value = "pageCount", defaultValue = "10") Integer pageCount) {
        LOG.info(" access /serverapi/task/search userId {} state {} start {} count {} ", userId, state, currentPage, pageCount);
        return taskService.search(userId, state, currentPage, pageCount);
    }


    @ApiOperation(value = "批量删除已完成任务")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
    public CommonResult<Object> batchDelete(@RequestParam("taskIds") List<String> taskIds) {
        LOG.info(" access /serverapi/task/batchDelete taskIDds {} ", taskIds);
        taskService.batchDelete(taskIds);
        return this.successResultWithEmptyResult();
    }

}
