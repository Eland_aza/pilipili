package com.pilipili.serverapi.controller.classifcation;

import com.pilipili.common.controller.AbstractController;
import com.pilipili.common.controller.CommonResult;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.serverapi.classification.dto.resp.ClassificationWithParentDto;
import com.pilipili.serverapi.classification.dto.resp.ClassificationsDto;
import com.pilipili.serverapi.classification.entity.Classification;
import com.pilipili.serverapi.classification.service.ClassificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 分类管理接口
 *
 * @author chen.guosheng
 */
@Api(description = "分类管理接口")
@RestController
@RequestMapping("/serverapi/classification")
public class ClassificationController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(ClassificationController.class);

    @Autowired
    private ClassificationService classificationService;

    @ApiOperation(value = "根据分类id获取分类和父类信息")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/getClassAndParentClass", method = RequestMethod.GET)
    public CommonResult<ClassificationWithParentDto> getClassAndParentClass(@RequestParam(value = "classificationId", defaultValue = "") Long classificationId) {
        LOG.info(" access /serverapi/classification/getClassAndParentClass classificationId ", classificationId);
        Classification classification = this.classificationService.find(classificationId);
        Classification parentClass = this.classificationService.find(classification.getParentClassId());
        if(parentClass != null)
        return successResult(new ClassificationWithParentDto(classification.getId(), classification.getClassificationName(), parentClass.getId(), parentClass.getClassificationName()));
        return successResult(new ClassificationWithParentDto(classification.getId(), classification.getClassificationName(), 0L, ""));
    }

    @ApiOperation(value = "获取所有分类信息")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/getAllClass", method = RequestMethod.GET)
    public CommonResult<ClassificationsDto> getAllClass() {
        LOG.info(" access /serverapi/classification/getAllClass");
        List<ClassificationsDto> classificationsDtos = classificationService.getAllClass();
        return successResult(classificationsDtos);
    }


}
