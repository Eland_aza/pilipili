package com.pilipili.serverapi.controller.collection;

import com.pilipili.common.Exception.BusinessRuntimeException;
import com.pilipili.common.controller.AbstractController;
import com.pilipili.common.controller.CommonResult;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.ErrorCode;
import com.pilipili.serverapi.collection.dto.req.CollectionDto;
import com.pilipili.serverapi.collection.dto.resp.CollectionResultDto;
import com.pilipili.serverapi.collection.entity.Collection;
import com.pilipili.serverapi.collection.entity.Favorites;
import com.pilipili.serverapi.collection.service.CollectionService;
import com.pilipili.serverapi.collection.service.FavoritesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 收藏条目管理接口
 *
 * @author chen.guosheng
 */
@Api(description = "收藏条目管理接口")
@RestController
@RequestMapping("/serverapi/collection")
public class CollectionController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(CollectionController.class);

    @Autowired
    private CollectionService collectionService;
    @Autowired
    private FavoritesService favoritesService;

    @ApiOperation(value = "新增收藏条目")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public PageCommResult<Object> add(@Valid @RequestBody CollectionDto collectionDto, BindingResult bindingResult) {
        LOG.info(" access /serverapi/collection/add collectionDto {} ", collectionDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        for(Long favoritesId : collectionDto.getFavoritesIds()) {
            if (collectionService.existCollection(collectionDto.getUserId(), collectionDto.getVideoId(), favoritesId)) {
                throw new BusinessRuntimeException(ErrorCode.COLLECTION_EXIST);
            }
        }
        for(Long favoritesId : collectionDto.getFavoritesIds()) {
            Collection collection = new Collection();
            collection.setUserId(collectionDto.getUserId());
            collection.setVideoId(collectionDto.getVideoId());
            collection.setFavoritesId(favoritesId);
            collectionService.save(collection);
        }
        return this.successPageResultWithEmptyResult();
    }

    @ApiOperation(value = "搜索收藏条目")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public PageCommResult<CollectionResultDto> search(@RequestParam(value = "favoritesId", defaultValue = "") Long favoritesId,
                                                      @RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
                                                      @RequestParam(value = "pageCount", defaultValue = "10") Integer pageCount) {
        LOG.info(" access /serverapi/collection/search favoritesId {} start {} count {} ", favoritesId, currentPage, pageCount);
        return collectionService.search(favoritesId, currentPage, pageCount);
    }

    @ApiOperation(value = "批量删除收藏条目")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
    public PageCommResult<Object> batchDelete(@RequestParam("collectionIds") List<Long> collectionIds) {
        LOG.info(" access /serverapi/collection/batchDelete collectionIds {} ", collectionIds);
        collectionService.batchDelete(collectionIds);
        return this.successPageResultWithEmptyResult();
    }

}
