package com.pilipili.serverapi.controller.manageUser;

import com.pilipili.common.Exception.BusinessRuntimeException;
import com.pilipili.common.controller.AbstractController;
import com.pilipili.common.controller.CommonResult;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.ErrorCode;
import com.pilipili.serverapi.manageUser.dto.req.ManageUserInfoDto;
import com.pilipili.serverapi.manageUser.dto.req.ManageUserLoginDto;
import com.pilipili.serverapi.manageUser.dto.req.ManageUserSighUpDto;
import com.pilipili.serverapi.manageUser.dto.resp.ManageLoginResultDto;
import com.pilipili.serverapi.manageUser.dto.resp.ManageUserInfoResultDto;
import com.pilipili.serverapi.manageUser.entity.ManageUser;
import com.pilipili.serverapi.manageUser.service.ManageUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 后台管理通用接口
 *
 * @author chen.guosheng
 */
@Api(description = "后台管理系统通用接口")
@RestController
@RequestMapping("/serverapi")
public class ManageUserController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(ManageUserController.class);

    @Autowired
    HttpServletRequest request;

    @Autowired
    ManageUserService manageUserService;

    @ApiOperation(value = "登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public CommonResult<ManageLoginResultDto> login(@Valid @RequestBody ManageUserLoginDto manageUserLoginDto,
                                                    BindingResult bindingResult){
        LOG.info(" access /serverapi/sighup manageUserLoginDto {} ", manageUserLoginDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        return successResult(manageUserService.login(manageUserLoginDto));
    }

    @ApiOperation(value = "注册")
    @RequestMapping(value = "/sighup", method = RequestMethod.POST)
    public CommonResult<ManageLoginResultDto> sighup(@Valid @RequestBody ManageUserSighUpDto manageUserSighUpDto,
                                                     BindingResult bindingResult){
        LOG.info(" access /serverapi/sighup manageUserSighUpDto {} ", manageUserSighUpDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        return successResult(manageUserService.addManageUser(manageUserSighUpDto));
    }

    @ApiOperation(value = "修改个人信息")
    @RequestMapping(value = "/editManageUserInfo", method = RequestMethod.POST)
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    public PageCommResult<Object> editManageUserInfo(@Valid @RequestBody ManageUserInfoDto manageUserInfoDto,
                                           BindingResult bindingResult){
        LOG.info(" access /serverapi/editManageUserInfo manageUserInfoDto {} ", manageUserInfoDto);
        if (bindingResult.hasErrors()) {
            throw new BusinessRuntimeException(ErrorCode.PRAMERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        manageUserService.editInfo(manageUserInfoDto);
        return successPageResultWithEmptyResult();
    }


    @ApiOperation(value = "根据用户id获取用户信息")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequestMapping(value = "/getManageUserInfo", method = RequestMethod.GET)
    public CommonResult<ManageUserInfoResultDto> getManageUserInfo(@RequestParam(value = "userId", defaultValue = "") Long userId) {
        LOG.info(" access /serverapi/getManageUserInfo userId ", userId);
        ManageUser manageUser = this.manageUserService.find(userId);
        ManageUserInfoResultDto manageUserInfoResultDto = new ManageUserInfoResultDto();
        if(manageUser == null) throw new BusinessRuntimeException(ErrorCode.USER_NOT_FOUND);
        BeanUtils.copyProperties(manageUser, manageUserInfoResultDto);
        return successResult(manageUserInfoResultDto);
    }



    @ApiOperation(value = "验证后台管理权限")
    @ApiImplicitParam(name="Token", value="用于验证登录的token", paramType="header",dataType="string")
    @RequiresPermissions("manage")
    @RequestMapping(value = "/manage/testAuth", method = RequestMethod.POST)
    public PageCommResult<Object> testAuth() {
        LOG.info(" access /serverapi/manage/testAuth");
        return this.successPageResultWithEmptyResult();
    }

    @ApiIgnore
    @RequestMapping(value = "/unlogin", method = RequestMethod.GET)
    public void unlogin(){
        throw new BusinessRuntimeException(ErrorCode.USER_NOT_LOGNIN);
    }

    @ApiIgnore
    @RequestMapping(value = "/unauth", method = RequestMethod.GET)
    public void unauth(){
        throw new BusinessRuntimeException(ErrorCode.UNAUTH);
    }

    @ApiIgnore
    @RequestMapping(value = "/kickout", method = RequestMethod.GET)
    public void kickout(){
        throw new BusinessRuntimeException(ErrorCode.TOKEN_INVAILD);
    }

    @ApiIgnore
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public void index(){
    }
}
