package com.pilipili.serverapi.controller.comment;


import com.pilipili.common.controller.AbstractController;
import com.pilipili.serverapi.controller.collection.FavoritesController;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 评论管理接口
 *
 * @author chen.guosheng
 */
@Api(description = "评论管理接口")
@RestController
@RequestMapping("/serverapi/comment")
public class CommentController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(CommentController.class);

}
