package com.pilipili.serverapi.video.repository;

import com.pilipili.serverapi.video.dto.req.VideoSearchDto;
import com.pilipili.serverapi.video.entity.Video;
import org.apache.commons.lang3.StringUtils;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
/**
 * 视频复杂查询Repository
 *
 * @author chen.guosheng
 */
public class VideoExtRepositoryImpl implements VideoExtRepository{

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Video> search(VideoSearchDto videoSearchDto) {
        String sql = " ";
        Query query = searchCommon(sql, videoSearchDto, Video.class);
        query.setFirstResult(videoSearchDto.getPage().getStart() * videoSearchDto.getPage().getCount());
        query.setMaxResults(videoSearchDto.getPage().getCount());
        return query.getResultList();
    }

    public <T> Query searchCommon(String querySQL, VideoSearchDto videoSearchDto, Class<T> resultClass){
        querySQL += " from Video v where 1 = 1 ";
        if(videoSearchDto.getId() != null){
            querySQL += " and v.id = :id ";
        }
        if(StringUtils.isNotBlank(videoSearchDto.getTitle())){
            querySQL += " and v.title like :title ";
        }
        if(videoSearchDto.getClassificationId() != null){
            querySQL += " and v.classificationId = :classificationId ";
        }
        if(videoSearchDto.getAuditState() != null){
            querySQL += " and v.auditState = :auditState ";
        }
        if(videoSearchDto.getTransState() != null){
            querySQL += " and v.transState = :transState ";
        }
        if(videoSearchDto.getUserId() != null){
            querySQL += " and v.userId = :userId ";
        }
        if(videoSearchDto.getOrderType() != null){
            if(videoSearchDto.getOrderType() == 0){
                querySQL += " order by v.createTime desc ";
            }
            else if(videoSearchDto.getOrderType() == 1){
                querySQL += " order by v.playTimes desc ";
            }
        }
        Query query = em.createQuery(querySQL, resultClass);
        if(videoSearchDto.getId() != null){
            query.setParameter("id", videoSearchDto.getId());
        }
        if(StringUtils.isNotBlank(videoSearchDto.getTitle())){
            query.setParameter("title", "%"+ videoSearchDto.getTitle() + "%");
        }
        if(videoSearchDto.getClassificationId() != null){
            query.setParameter("classificationId", videoSearchDto.getClassificationId());
        }
        if(videoSearchDto.getAuditState() != null){
            query.setParameter("auditState", videoSearchDto.getAuditState());
        }
        if(videoSearchDto.getTransState() != null){
            query.setParameter("transState", videoSearchDto.getTransState());
        }
        if(videoSearchDto.getUserId() != null){
            query.setParameter("userId", videoSearchDto.getUserId());
        }
        return query;
    }

    public Long countSearch(VideoSearchDto videoSearchDto){
        String querySQL = "select count(*)  ";
        Query query = searchCommon(querySQL, videoSearchDto, Long.class);
        return (Long) query.getSingleResult();
    }
}
