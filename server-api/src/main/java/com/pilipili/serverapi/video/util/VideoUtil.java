package com.pilipili.serverapi.video.util;

import com.pilipili.serverapi.video.dto.req.VideoUploadDto;
import com.pilipili.serverapi.video.entity.Video;
import com.pilipili.serverapi.video.service.TaskService;
import lombok.Data;
import org.mountcloud.ffmepg.operation.ffmpeg.vidoe.FFMpegVideoFormatM3u8;
import org.mountcloud.ffmepg.operation.ffmpeg.vidoe.FFMpegVideoScreenShot;
import org.mountcloud.ffmepg.task.bean.FFTask;
import org.mountcloud.ffmepg.task.bean.FFTaskStateEnum;
import org.mountcloud.ffmepg.task.bean.tasks.FFMepgVideoFormatM3u8Task;
import org.mountcloud.ffmepg.task.bean.tasks.FFMpegVideoScreenShotTask;
import org.mountcloud.ffmepg.task.context.FFTaskContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
/**
 * 视频转码与视频截图封面
 *
 * @author chen.guosheng
 */
@Data
@Component
@PropertySource({"classpath:application.yml"})
public class VideoUtil {

    public static Map<Video, FFMepgVideoFormatM3u8Task> taskMap = new HashMap<Video,FFMepgVideoFormatM3u8Task>();

    @Value("${savePath.videoRequestPath}")
    private String videoRequestPath;

    @Value("${savePath.videoOriFilePath}")
    private String videoOriFilePath;

    @Value("${savePath.videoFilePath}")
    private String videoFilePath;

    public VideoUtil(){
        TaskManageThread taskManageThread = new TaskManageThread();
        Thread thread = new Thread(taskManageThread);
        thread.start();
        System.out.println("任务线程启动");
    }

    public void transcodeToM3U8(MultipartFile videoOriFile, Video video){

        String[] str = videoOriFile.getOriginalFilename().split("\\.");
        String fileName = str[0];

        String oriFilePath = videoOriFilePath + video.getUserId() + "/" + videoOriFile.getOriginalFilename();
        String m3u8FilePath = videoFilePath + video.getUserId() + "_" + fileName + "/" + fileName + ".m3u8";
        String tsFilePath = videoFilePath + video.getUserId() + "_" + fileName + "/" + fileName + "%5d.ts";

        FFMpegVideoFormatM3u8 m3u8Operation = new FFMpegVideoFormatM3u8();
        m3u8Operation.setVideoFileName(oriFilePath);
        m3u8Operation.setBitrate("500k");
        m3u8Operation.setTimes(5);
        File file = new File(m3u8FilePath);
        File parentFile = file.getParentFile();
        if(!parentFile.exists()){
            parentFile.mkdirs();
        }
        m3u8Operation.setM3u8File(m3u8FilePath);
        m3u8Operation.setTsFiles(tsFilePath);

        FFMepgVideoFormatM3u8Task task = new FFMepgVideoFormatM3u8Task(m3u8Operation);
        FFTaskContext.getContext().addTask(task);
        System.out.println("taskID:"+task.getTaskId());
        taskMap.put(video, task);
        //生成视频截图，默认截取视频第一帧
        String previewImagePath = videoFilePath + video.getUserId() + "_" + fileName + "/" + fileName + ".jpg";
        System.out.println(oriFilePath);
        System.out.println(previewImagePath);
        screenShot(oriFilePath, previewImagePath);
    }

    public void screenShot(String videoOriFilePath, String previewImagePath){
        String startTime = "0";
        String vframes = "1";
        FFMpegVideoScreenShot ffMpegVideoScreenShot = new FFMpegVideoScreenShot(videoOriFilePath,startTime,vframes,previewImagePath);
        FFMpegVideoScreenShotTask screenShotTask = new FFMpegVideoScreenShotTask(ffMpegVideoScreenShot);
        FFTaskContext.getContext().submit(screenShotTask);
    }

}
