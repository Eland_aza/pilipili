package com.pilipili.serverapi.video.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 视频实体类
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Video implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String introduction;
    private String videoOri;
    private String videoUrl;
    private String previewImageUrl;
    private Long classificationId;
    private Long playTimes;
    private Long auditState;
    private Long transState;
    private Long userId;
    private Date createTime;
}
