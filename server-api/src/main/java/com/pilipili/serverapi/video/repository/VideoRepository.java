package com.pilipili.serverapi.video.repository;

import com.pilipili.serverapi.video.entity.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 视频管理Repository
 *
 * @author chen.guosheng
 */
@Repository
public interface VideoRepository extends JpaRepository<Video, Long>, VideoExtRepository {

    /**
     * 根据上传者id查找
     * @param userId
     * @return
     */
    List<Video> findByUserId(Long userId);


    /**
     * 根据上传者id和视频标题查找
     * @param
     * @return
     */
    Video findByUserIdAndTitle(Long userId, String title);

    /**
     * 根据视频id批量删除视频
     *
     * @param videoIds
     */
    void deleteByIdIn(Collection<Long> videoIds);

}
