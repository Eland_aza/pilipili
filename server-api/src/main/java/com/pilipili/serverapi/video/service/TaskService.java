package com.pilipili.serverapi.video.service;


import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.service.AbstractService;
import com.pilipili.common.util.BeanUtils;
import com.pilipili.serverapi.video.dto.resp.TaskWithVideoTitleDto;
import com.pilipili.serverapi.video.entity.Task;
import com.pilipili.serverapi.video.entity.Video;
import com.pilipili.serverapi.video.repository.TaskRepository;
import com.pilipili.serverapi.video.repository.VideoRepository;
import com.pilipili.serverapi.video.util.VideoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 任务管理Service
 *
 * @author chen.guosheng
 */
@Service
public class TaskService extends AbstractService<TaskRepository, Task, String> {

    @Autowired
    private VideoService videoService;

    @Transactional
    public PageCommResult<TaskWithVideoTitleDto> search(Long userId, Long state, int currentPage, int pageCount){
        Pageable page = new PageRequest(currentPage, pageCount, Sort.Direction.DESC, "createTime");
        List<Task> tasks = null;
        if(state == -2) tasks = this.repository.findByUserId(userId, page);
        if(state != -2) tasks = this.repository.findByUserIdAndState(userId, state, page);
        List<TaskWithVideoTitleDto> taskWithVideoTitleDtos = new ArrayList<>();
        for(Task task : tasks){
            Video video = videoService.find(task.getVideoId());
            TaskWithVideoTitleDto taskWithVideoTitleDto = new TaskWithVideoTitleDto();
            BeanUtils.copyProperties(task, taskWithVideoTitleDto);
            taskWithVideoTitleDto.setTitle(video.getTitle());
            taskWithVideoTitleDtos.add(taskWithVideoTitleDto);
        }
        Long total = this.repository.countByUserId(userId);
        return PageCommResult.successPageResult(taskWithVideoTitleDtos, currentPage, pageCount, total);
    }

    @Transactional
    public Task findByTaskId(String taskId){
        Optional<Task> taskOptional = this.repository.findById(taskId);
        if(taskOptional.isPresent()){
            return taskOptional.get();
        }
        return null;
    }

    @Transactional
    public List<Task> findByVideoId(Long videoId){
       return this.repository.findByVideoId(videoId);
    }

    @Transactional
    public void batchDelete(List<String> taskIds){
        for(String taskId : taskIds){
            Task task = this.find(taskId);
            for(Video video : VideoUtil.taskMap.keySet()){
                if(video.getId().equals(task.getVideoId())){
                    VideoUtil.taskMap.remove(video);
                }
            }
        }
        this.repository.deleteByStateAndTaskIdIn(0L, taskIds);
    }
}
