package com.pilipili.serverapi.video.repository;

import com.pilipili.serverapi.video.entity.Task;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 任务管理Repository
 *
 * @author chen.guosheng
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, String> {


    /**
     * 根据userId查找任务
     * @param userId
     * @param pageable
     * @return
     */
    List<Task> findByUserId(Long userId, Pageable pageable);

    List<Task> findByUserIdAndState(Long userId, Long state, Pageable pageable);

    /**
     * 根据videoId查找任务
     * @param videoId
     * @return
     */
    List<Task> findByVideoId(Long videoId);

    /**
     * 统计数量
     * @param userId
     * @return
     */
    Long countByUserId(Long userId);

    /**
     * 批量删除已完成任务
     *
     * @param taskIds
     */
    void deleteByStateAndTaskIdIn(Long state, Collection<String> taskIds);




}
