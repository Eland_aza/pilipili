package com.pilipili.serverapi.video.dto.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 上传视频信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "VideoUploadDto", description = "上传视频信息")
@Data
@NoArgsConstructor
public class VideoUploadDto {

    @ApiModelProperty(value = "视频标题", required = true)
    @NotBlank(message = "标题不能为空")
    private String title;

    @ApiModelProperty(value = "视频简介")
    private String introduction;

    @ApiModelProperty(value = "分类id")
    @NotNull(message = "分类id不能为空")
    private Long classificationId;

    @ApiModelProperty(value = "上传者id")
    @NotNull(message = "上传者id不能为空")
    private Long userId;

}
