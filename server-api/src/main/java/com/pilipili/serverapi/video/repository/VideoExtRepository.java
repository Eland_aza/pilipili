package com.pilipili.serverapi.video.repository;

import com.pilipili.serverapi.video.dto.req.VideoSearchDto;
import com.pilipili.serverapi.video.entity.Video;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 视频复杂查询Repository
 *
 * @author chen.guosheng
 */
@Repository
public interface VideoExtRepository {
    /**
     * 视频多条件查找
     * @param videoSearchDto
     * @return
     */
    List<Video> search(VideoSearchDto videoSearchDto);

    Long countSearch(VideoSearchDto videoSearchDto);
}
