package com.pilipili.serverapi.video.dto.resp;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 任务信息包含视频标题
 *
 * @author chen.guosheng
 */
@ApiModel(value = "TaskWithVideoTitleDto", description = "搜索任务信息包括标题")
@Data
@NoArgsConstructor
public class TaskWithVideoTitleDto {

    @ApiModelProperty(value = "任务id")
    private String taskId;

    @ApiModelProperty(value = "视频标题")
    private String title;

    @ApiModelProperty(value = "上传者id")
    private Long userId;

    @ApiModelProperty(value = "转码进度")
    private Long process;

    @ApiModelProperty(value = "转码状态")
    private Long state;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

}
