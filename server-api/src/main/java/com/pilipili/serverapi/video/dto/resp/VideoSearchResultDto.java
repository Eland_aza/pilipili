package com.pilipili.serverapi.video.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 上传视频信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "VideoSearchResultDto", description = "搜索视频结果信息")
@Data
@NoArgsConstructor
public class VideoSearchResultDto {

    @ApiModelProperty(value = "视频id")
    private Long id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "视频简介")
    private String introduction;

    @ApiModelProperty(value = "视频源文件")
    private String videoOri;

    @ApiModelProperty(value = "视频url")
    private String videoUrl;

    @ApiModelProperty(value = "封面url")
    private String previewImageUrl;

    @ApiModelProperty(value = "分类id")
    private Long classificationId;

    @ApiModelProperty(value = "播放次数")
    private Long playTimes;

    @ApiModelProperty(value = "审核状态")
    private Long auditState;

    @ApiModelProperty(value = "转码状态")
    private Long transState;

    @ApiModelProperty(value = "上传者id")
    private Long userId;

    @ApiModelProperty(value = "上传时间")
    private String createTime;
}
