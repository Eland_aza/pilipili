package com.pilipili.serverapi.video.util;

import com.pilipili.serverapi.config.BeanContext;
import com.pilipili.serverapi.video.entity.Task;
import com.pilipili.serverapi.video.entity.Video;
import com.pilipili.serverapi.video.service.TaskService;
import com.pilipili.serverapi.video.service.VideoService;

import org.mountcloud.ffmepg.task.bean.FFTaskStateEnum;
import org.mountcloud.ffmepg.task.bean.tasks.FFMepgVideoFormatM3u8Task;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 扫描任务线程，更新任务列表状态
 *
 * @author chen.guosheng
 */
@Component
public class TaskManageThread implements Runnable{

    private TaskService taskService;

    private VideoService videoService;

    private M3u8FileHandler m3u8FileHandler;

    private Set<String> m3u8FilePathSet;

    @Override
    public void run() {
        while(true){
            if(taskService == null) this.taskService = BeanContext.getApplicationContext().getBean(TaskService.class);
            if(videoService == null) this.videoService = BeanContext.getApplicationContext().getBean(VideoService.class);
            if(m3u8FileHandler == null) this.m3u8FileHandler = BeanContext.getApplicationContext().getBean(M3u8FileHandler.class);
            if(m3u8FilePathSet == null) this.m3u8FilePathSet = new HashSet<String>();
            try {
                if(VideoUtil.taskMap!= null && !VideoUtil.taskMap.isEmpty()){
                    for(Map.Entry<Video, FFMepgVideoFormatM3u8Task> entry : VideoUtil.taskMap.entrySet()){
                        FFMepgVideoFormatM3u8Task task = entry.getValue();
                        Video video = entry.getKey();
                        try {
//                            System.out.println(task.getTaskId());
                            Task task2 = taskService.findByTaskId(task.getTaskId());
                            if(task2 != null){
                                switch (task.getProgress().getState().getValue()){
                                    case 0: task2.setState(0L);break;
                                    case 1: task2.setState(1L);break;
                                    case -1: task2.setState(-1L);break;
                                    case 2: task2.setState(2L);break;
                                }
                                task2.setProcess((long)task.getProgress().getProgress());
                                taskService.save(task2);
                                if(task.getProgress().getState().equals(FFTaskStateEnum.COMPLETE)){
                                    Video video1 = videoService.find(video.getId());
                                    if(video1 != null) {
                                        video1.setTransState(1L);
                                        videoService.save(video1);
                                    }
                                    String str[] = video.getVideoOri().split("\\.");
                                    String fileName = str[0];
                                    String filePath = video.getUserId() + "_" + fileName + "/" + fileName + ".m3u8";
                                    if(!m3u8FilePathSet.contains(filePath)) {
                                        m3u8FileHandler.handleM3u8File(filePath);
                                        m3u8FilePathSet.add(filePath);
                                    }
                                }
//                                System.out.println("更新转码进度");
                            }
                            else{
                                Task task1 = new Task();
                                task1.setTaskId(task.getTaskId());
                                task1.setVideoId(video.getId());
                                task1.setUserId(video.getUserId());
                                task1.setProcess(0L);
                                task1.setState(-1L);
                                taskService.save(task1);
                            }
                        } catch (NullPointerException e) {
                            System.out.println("task异常");
                            e.printStackTrace();
                        }
                    }
                }
//                System.out.println("task线程正常运行");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
