package com.pilipili.serverapi.video.dto.req;


import com.pilipili.common.dto.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 上传视频信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "VideoSearchDto", description = "搜索视频信息")
@Data
@NoArgsConstructor
public class VideoSearchDto {

    @ApiModelProperty(value = "视频id")
    private Long id;

    @ApiModelProperty(value = "视频标题")
    private String title;

    @ApiModelProperty(value = "分类id")
    private Long classificationId;

    @ApiModelProperty(value = "审核状态，0为未审核，-1为审核不通过，1为审核通过")
    private Long auditState;

    @ApiModelProperty(value = "转码状态，0为未转码完成，1为转码成功")
    private Long transState;

    @ApiModelProperty(value = "上传者id")
    private Long userId;

    @ApiModelProperty(value = "排序类型，0为按时间降序，1为播放量降序")
    @NotNull(message = "排序类型不能为空")
    private Long orderType;

    @ApiModelProperty(value = "分页信息")
    private PageParam page = new PageParam();

}
