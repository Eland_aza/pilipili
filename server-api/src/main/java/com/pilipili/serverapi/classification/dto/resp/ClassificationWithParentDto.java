package com.pilipili.serverapi.classification.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotNull;

/**
 * 分类与父类信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "ClassificationWithParentDto", description = "分类与父类信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassificationWithParentDto {

    @ApiModelProperty(value = "分类id")
    private Long id;

    @ApiModelProperty(value = "分类名")
    private String classificationName;

    private Long parentId;

    @ApiModelProperty(value = "父类名")
    private String parentClassName;

}
