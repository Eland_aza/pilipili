package com.pilipili.serverapi.classification.repository;

import com.pilipili.serverapi.classification.entity.Classification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 分类管理Repository
 *
 * @author chen.guosheng
 */

@Repository
public interface ClassificationRepository extends JpaRepository<Classification, Long> {

    /**
     * 根据父类id查找分类
     * @param parentClassId
     * @return
     */
    List<Classification> findByParentClassId(Long parentClassId);

}
