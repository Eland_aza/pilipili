package com.pilipili.serverapi.classification.dto.resp;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 所有分类信息
 *
 * @author chen.guosheng
 */
@ApiModel(value = "ClassificationsDto", description = "所有分类信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassificationsDto {

    @ApiModelProperty(value = "分类id")
    private Long id;

    @ApiModelProperty(value = "分类名")
    private String classificationName;

    @ApiModelProperty(value = "子类列表")
    private List<ClassificationsDto> classificationsDtos;

}
