package com.pilipili.serverapi.classification.service;


import com.pilipili.common.controller.CommonResult;
import com.pilipili.common.controller.PageCommResult;
import com.pilipili.common.service.AbstractService;
import com.pilipili.serverapi.classification.dto.resp.ClassificationsDto;
import com.pilipili.serverapi.classification.entity.Classification;
import com.pilipili.serverapi.classification.repository.ClassificationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 分类管理Service
 *
 * @author chen.guosheng
 */
@Service
public class ClassificationService extends AbstractService<ClassificationRepository, Classification, Long> {

    public List<ClassificationsDto> getAllClass(){
        List<Classification> classifications = this.repository.findByParentClassId(0L);
        List<ClassificationsDto> classificationsDtos = new ArrayList<>();
        for(Classification classification : classifications){
            ClassificationsDto classificationsDto = new ClassificationsDto();
            classificationsDto.setId(classification.getId());
            classificationsDto.setClassificationName(classification.getClassificationName());
            classificationsDto.setClassificationsDtos(new ArrayList<>());
            List<Classification> childClassifications = this.repository.findByParentClassId(classification.getId());
            for(Classification childClassification : childClassifications){
                ClassificationsDto childClassificationsDto = new ClassificationsDto();
                childClassificationsDto.setId(childClassification.getId());
                childClassificationsDto.setClassificationName(childClassification.getClassificationName());
                classificationsDto.getClassificationsDtos().add(childClassificationsDto);
            }
            classificationsDtos.add(classificationsDto);
        }
        return classificationsDtos;
    }

}
