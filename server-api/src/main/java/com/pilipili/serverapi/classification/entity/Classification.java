package com.pilipili.serverapi.classification.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 视频实体类
 * @author chen.guosheng
 */
@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Classification implements Serializable {
    @Id
    private Long id;
    private String classificationName;
    private Long parentClassId;
    private Date createTime;
}
