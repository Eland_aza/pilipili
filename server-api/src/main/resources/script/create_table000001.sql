-- liquibase formatted sql

-- changeset pilipili:create_table00001-1
create table if not exists `manage_user`(
	id int not null primary key auto_increment,
	user_name varchar(20) not null default '' comment '用户名',
	password varchar(20) not null default '' comment '密码',
	mobile varchar(15) not null default '' comment '手机号',
	email varchar(20) not null default '' comment '邮箱',
	create_time timestamp not null default  CURRENT_TIMESTAMP comment '注册时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '后台用户表';
-- rollback drop table `manage_user`;

-- changeset pilipili:create_table00001-2
create table if not exists `user_role`(
	id int not null primary key auto_increment,
	user_id int not null comment '用户id',
	role_id int not null comment '角色id',
	create_time timestamp not null default  CURRENT_TIMESTAMP comment '注册时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '用户角色表';
-- rollback drop table `user_role`;

-- changeset pilipili:create_table00001-3
create table if not exists `role`(
	id int not null primary key auto_increment,
	role_name varchar(20) not null default '' comment '角色名',
	description varchar(20) not null default '' comment '角色描述',
	create_time timestamp not null default  CURRENT_TIMESTAMP comment '注册时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '角色表';
-- rollback drop table `role`;

-- changeset pilipili:create_table00001-4
create table if not exists `role_permission`(
	id int not null primary key auto_increment,
	role_id int not null comment '角色id',
	permission_id int not null comment '权限id',
	create_time timestamp not null default  CURRENT_TIMESTAMP comment '注册时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '角色权限表';
-- rollback drop table `role_permission`;

-- changeset pilipili:create_table00001-5
create table if not exists `permission`(
	id int not null primary key auto_increment,
	permission_name varchar(20) not null default '' comment '权限名',
	description varchar(20) not null default '' comment '权限描述',
	create_time timestamp not null default  CURRENT_TIMESTAMP comment '注册时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '角色表';
-- rollback drop table `permission`;

-- changeset pilipili:create_table00001-6
alter table `permission` add column `permission_url` varchar(1024) NOT NULL default '' COMMENT '权限url' after `description`;
-- rollback ALTER TABLE `commodity` DROP COLUMN `permission_url`;

-- changeset pilipili:create_table00001-7
alter table `permission` add column `parent_id` int NOT NULL default '' COMMENT '父权限id' after `description`;
-- rollback ALTER TABLE `commodity` DROP COLUMN `parent_id`;

-- changeset pilipili:create_table00001-8
create table if not exists `video` (
  id int not null primary key auto_increment,
  title varchar(20) not null default '' comment '视频标题',
  introduction varchar(256) not null default '' comment '视频简介',
  video_ori varchar(20) not null default '' comment '视频源文件名',
  video_url varchar(256) not null default '' comment 'm3u8视频地址',
  preview_image_url varchar(256) not null default  '' comment '视频封面地址',
  classification_id int not null default 0 comment '分类id',
  play_times int not null default 0 comment '播放次数',
  audit_state int not null default 0 comment '审核状态，0为未审核，-1为审核不通过，1为审核通过',
  trans_state int not null default 0 comment '转码状态，0为未转码完成，1为转码成功',
  user_id int not null comment '上传者id',
  create_time timestamp not null default  CURRENT_TIMESTAMP comment '上传/更新时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '视频表';
-- rollback drop table `Video`;

-- changeset pilipili:create_table00001-9
create table if not exists `classification`(
  id int not null primary key auto_increment,
  classification_name varchar(20) not null default '' comment '分类名',
  parent_class_id int not null default 0 comment '父分类id',
  create_time timestamp not null default  CURRENT_TIMESTAMP comment '创建时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '分类表';
-- rollback drop table `classification`;

-- changeset pilipili:create_table00001-10
create table if not exists `task`(
  task_id int not null primary key,
  video_id int not null comment '视频id',
  user_id int not null comment '上传者id',
  process int not null default 0 comment '进度，范围为0-100',
  state int not null default 2 comment '任务状态，2为在排队中，1为正在转码，0为已完成。-1为转码失败',
  create_time timestamp not null default  CURRENT_TIMESTAMP comment '创建时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '分类表';
-- rollback drop table `task`;

-- changeset pilipili:create_table00001-11
alter table task modify column task_id varchar(128);
-- rollback ALTER TABLE `commodity` DROP COLUMN `parent_id`;

-- changeset pilipili:create_table00001-12
alter table video modify column video_ori varchar(128);
-- rollback ALTER TABLE `commodity` DROP COLUMN `parent_id`;

-- changeset pilipili:create_table00001-13
create table if not exists `collection`(
  id int not null primary key auto_increment,
  user_id int not null comment '用户id',
  video_id int not null comment '视频id',
  favorites_id int not null comment '收藏夹id',
  create_time timestamp not null default  CURRENT_TIMESTAMP comment '创建时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '收藏表';
-- rollback drop table `collection`;

-- changeset pilipili:create_table00001-14
create table if not exists `favorites`(
  id int not null primary key auto_increment,
  user_id int not null comment '用户id',
  favorites_name varchar(256) not null default '' comment '收藏夹名称',
  introduction varchar(256) not null default '' comment '视频简介',
  create_time timestamp not null default  CURRENT_TIMESTAMP comment '创建时间'
)CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci comment '收藏夹表';
-- rollback drop table `favorites`;