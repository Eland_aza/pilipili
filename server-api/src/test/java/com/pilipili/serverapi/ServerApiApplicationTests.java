package com.pilipili.serverapi;

import com.pilipili.serverapi.manageUser.entity.ManageUser;
import com.pilipili.serverapi.manageUser.entity.Permission;
import com.pilipili.serverapi.manageUser.entity.Role;
import com.pilipili.serverapi.manageUser.repository.ManageUserRepository;
import com.pilipili.serverapi.manageUser.service.ManageUserService;
import com.pilipili.serverapi.video.util.M3u8FileHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mountcloud.ffmepg.operation.ffmpeg.vidoe.FFMpegVideoFormatM3u8;
import org.mountcloud.ffmepg.operation.ffmpeg.vidoe.FFMpegVideoInfo;
import org.mountcloud.ffmepg.result.defaultResult.FFVideoInfoResult;
import org.mountcloud.ffmepg.task.bean.FFTaskStateEnum;
import org.mountcloud.ffmepg.task.bean.tasks.FFMepgVideoFormatM3u8Task;
import org.mountcloud.ffmepg.task.bean.tasks.FFMepgVideoInfoTask;
import org.mountcloud.ffmepg.task.context.FFTaskContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServerApiApplication.class})
public class ServerApiApplicationTests {

    @Autowired
    private ManageUserRepository manageUserRepository;
    @Autowired
    private ManageUserService manageUserService;

    public void testUserRepository() {
        ManageUser manageUser = manageUserRepository.findByUserName("admin");
        for(Role role : manageUser.getRoles()){
            for(Permission permission : role.getPermissions()){
                System.out.println(permission.getPermissionName());
            }
        }
    }

    public void testFindRoleAndPermission(){
        Set<Role> roles = manageUserService.findRolesByUserId(1L);
        Set<Permission> permissions = manageUserService.findPermissionsByUserId(1L);
        for(Role role : roles){
            System.out.println(role.getRoleName() + "  " + role.getDescription());
        }
        for(Permission permission : permissions){
            System.out.println(permission.getPermissionName() + "  " + permission.getDescription());
        }
    }

    @Test
    public void testM3u8FileHandler(){
        M3u8FileHandler m3u8FileHandler = new M3u8FileHandler();
        m3u8FileHandler.handleM3u8File("1_那些年，我们一起追的女孩/那些年，我们一起追的女孩.m3u8");
    }

}
